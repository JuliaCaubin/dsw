<?php

$oldNombre = $_POST['nombre'] ?? "";
$oldApellido = $_POST['apellido'] ?? "";
$oldEmail = $_POST['email'] ?? "";
$oldSubject = $_POST['subject'] ?? "";
$oldMessage = $_POST['mensa'] ?? "";

$Errores = [];

if ($_SERVER["REQUEST_METHOD"]=="POST") {
    if(empty($oldNombre)){
        array_push($Errores, "Tienes que poner el nombre");
    }
    if(empty($oldSubject)){
        array_push($Errores, "Tienes que poner el tema");
    }
    if(!filter_var($oldEmail, FILTER_VALIDATE_EMAIL) || empty($oldEmail)){
        array_push($Errores, "Tienes que poner un email valido");
    }

}

require __DIR__."/utils/utils.php";
require "views/contact.view.php";

?>