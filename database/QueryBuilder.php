<?php
require __DIR__."/../exceptions/QueryException.php";
require __DIR__."/../exceptions/NotFoundException.php";

abstract class QueryBuilder{

    private $connection;
    private $table;
    private $classEntity;

    public function __construct(string $table, string $classEntity)
    {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }
    
    public function findAll(){
        $sql = "SELECT * FROM $this->table";
        
        $find = $this->executeQuery($sql);

        if (empty($find)) {
            throw new QueryException("No se ha podido ejecutar la consulta.");
        }

        return $find;
    }
    
    public function save(IEntity $entity): void{
        try{
            //guarda en una variable el metodo toArray
            $parameters = $entity->toArray();
            //crea una sentencia sql en la que inserta en una tabla los valores que se le pasan 
            $sql = sprintf("insert into %s (%s) values (%s)",
                $this->table,
                implode(",", array_keys($parameters)),
                ":".implode(", :", array_keys($parameters))
            );
            
            //accede a la coneccion y prepara la sentencia sql
            $statement = $this->connection->prepare($sql);
            //con la connecion hecha y la sentencia peparada llama al metodo to arry
            $statement->execute($parameters);
        }catch (PDOException $exception){
            //si no se inserta salta un error
            throw new QueryException("Error al insertar en la BBDD.");
        }
    }

    public function executeQuery(string $sql): array{
        $pdoStatement = $this->connection->prepare($sql);
        if($pdoStatement->execute()===false){
            throw new QueryException("No se ha podido ejecutar la consulta");
        }
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity);
    }

    public function find (int $id): IEntity{
        $sql = "SELECT * FROM $this->table where id=$id";
        $result = $this->executeQuery($sql);
        if (empty($result)){
            throw new NotFoundException("No se ha encontrado el elemento con id $id");
        }
        return $result[0];
    }
}


?>