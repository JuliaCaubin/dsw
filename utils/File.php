<?php

class File
{
    private $file;
    private $fileName;

    public function __construct(string $fileName, array $arrTypes)
    {
        $this->file = $_FILES[$fileName];
        $this->fileName = "";

        if ($this->file["name"] == "") {
            throw new FileException("Debes especificar un fichero", 1);
            
        }

        if ($this->file["error"] !== UPLOAD_ERR_OK) {
            switch ($this->file["error"]) {
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new FileException("El fichero excede el tamaño permitido", 2);
                    break;
                case UPLOAD_ERR_PARTIAL:
                    throw new FileException("El fichero esta incompleto", 2);
                    break;
                
                default:
                throw new FileException("Ha habido un error al subir el fichero", 2);
                    break;
            }
        }

        if (in_array($this->file["type"], $arrTypes)===false){
            throw new FileException("Sube un archivo compatible", 3);
        }
    }

    public function saveUploadFile(String $ruta){
        if(!is_uploaded_file($this->file["tmp_name"])){
            throw new FileException("El archivo no se ha subido mediante un formulario", 4);
        }
        $rutaDestino = $ruta.$this->file["name"];
        if(is_file($rutaDestino)){
            $idUnico = time();
            $this->file["name"] = $idUnico.$this->file["name"];
        }
        if (move_uploaded_file($this->file["tmp_name"], $ruta.$this->file["name"]) == FALSE) {
            throw new FileException("No se ha podido mover el fichero al destino especificado", 4);
        }
    }

    public function copyFile($origen, $destino){
        if(is_file($origen.$this->file["name"]) == false){
            throw new FileException("No existe el fichero ".$origen);
        }

        if(is_file($destino.$this->file["name"])){
            throw new FileException("El fichero ".$destino." ya existe");
        }else{
            if(copy($origen.$this->file["name"], $destino.$this->file["name"]) == false){
                throw new FileException("No se ha podido copiar el fichero.");
            }
        }

    }

    /**
     * Get the value of fileName
     */ 
    public function getFileName()
    {
        return $this->file["name"];
    }
}

?>
