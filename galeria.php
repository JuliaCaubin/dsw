<?php
require __DIR__."/utils/utils.php";
require __DIR__."/exceptions/FileException.php";
require __DIR__."/utils/File.php";
require __DIR__."/database/Connection.php";
require __DIR__."/database/QueryBuilder.php";
require __DIR__."/repository/ImagenGaleriaRepository.php";
require __DIR__."/repository/CategoriaRepository.php";
require __DIR__."/entity/ImagenGaleria.php";
require __DIR__."/entity/Categoria.php";
require __DIR__."/exceptions/AppExceptions.php";
require __DIR__."/core/App.php";

$descripcion = '';
try {
    $config =require_once("app/config.php");
    App::bind("config", $config);
    $imagenGaleriaRepository = new ImagenGaleriaRepository();
    $categoriaRepository = new CategoriaRepository();

    if ($_SERVER["REQUEST_METHOD"]==="POST") {
        
        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
        $imagen = new File("imagen", $tiposAceptados);
        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALERIA);
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALERIA, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);
        $categoria = trim(htmlspecialchars($_POST["categoria"]));
        $imagenGaleria = new ImagenGaleria(0,$imagen->getFileName(), $categoria, $descripcion);
        $imagenGaleriaRepository->save($imagenGaleria);
        $mensaje = "Se ha guardado la imagen en la BBDD.";
    }
    $imagenes = $imagenGaleriaRepository->findAll();
    $categorias = $categoriaRepository->findAll();

} catch (FileException $fileException) {
    $errores[] = $fileException->getMessage();

} catch (QueryException $queryException) {
$errores[] = $queryException->getMessage();

} catch (PDOException $pdoException) {
    $errores[] = $pdoException->getMessage();

}catch (AppException $appException) {
    $errores[] = $appException->getMessage();
}



require "views/galeria.view.php";
?>