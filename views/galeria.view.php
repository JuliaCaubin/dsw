<?php
//se le incluye el header y el nav de los partials
include __DIR__ . "/partials/inicio-doc.part.php";
include __DIR__ . "/partials/nav.part.php";
?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <!-- El metodo de peticion para acceder a la pagina es POST -->
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
            <!-- Si no hay errores se imprime info, su hay errores se imprime danger -->
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <!-- Si no hay errores se imprime un mensaje -->
                <?php if (empty($errores)) : ?>
                <p><?= $mensaje ?></p>
                <!-- Si hay errores se imprime una lista con todos los errores -->
                <?php else : ?>
                <ul>
                    <?php foreach ($errores as $error) : ?>
                    <li><?= $error ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <!-- El formulario con el boton para subir archivos, el PHP_SELF el nombre del script que se esta ejecutando, relativa al directorio raiz --> 
            <form class="form-horizontal" action="<?=$_SERVER["PHP_SELF"] ?>" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Categoria</label>
                        <select class="form-control" name="categoria">
                        <?php foreach ($categorias as $categoria) : ?>
                        <option value="<?= $categoria->getId() ?>"><?= $categoria->getNombre(); ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion"><?= $descripcion ?></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <table class = "table">
            <?php foreach ($imagenes as $imagen) : ?>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Imagen</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Descripcion</th>
                    <th scope="col">NumVisualizaciones</th>
                    <th scope="col">NumLikes</th>
                    <th scope="col">NumDownloads</th>
                </tr>
                <tr>
                    <th scope="row"><?= $imagen->getId() ?></th>
                    <td>
                        <img src="<?= $imagen->getURLGaleria() ?>" alt="<?= $imagen->getDescripcion() ?>" title="<?= $imagen->getDescripcion() ?>" width="100px">
                    </td>
                    <td><?= $imagen->getNombre() ?></td>
                    <td><?= $imagenGaleriaRepository->getCategoria($imagen)->getNombre() ?></td>
                    <td><?= $imagen->getDescripcion() ?></td>
                    <td><?= $imagen->getNumVisualizaciones() ?></td>
                    <td><?= $imagen->getNumLikes() ?></td>
                    <td><?= $imagen->getNumDownloads() ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
</div>
<!-- Principal Content End -->
<!-- Se le añade el footer y los scripts del final -->
<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>