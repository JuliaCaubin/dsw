<?php include __DIR__."/partials/inicio-doc.part.php"; ?>
<?php include __DIR__."/partials/nav.part.php"; ?>

<!-- Principal Content Start -->
   <div id="contact">
   	  <div class="container">
   	    <div class="col-xs-12 col-sm-8 col-sm-push-2">
       	   <h1>CONTACT US</h1>
       	   <hr>
       	   <p>Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
	       <form class="form-horizontal" method ="POST">
	       	  <div class="form-group">
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control" name="nombre">First Name</label>
	       	  		<input class="form-control" type="text" name="nombre" value="<?= $oldNombre ?>">
					<ul style="color:#5bd5db">
				<?php
                    if (!empty($Errores)) {
                    	foreach ($Errores as $Error) {
                            if ($Error == "Tienes que poner el nombre") {
                                echo "<li>$Error</li>";
                            }
                        }
                    }
           		?>
				   </ul>
	       	  	</div>
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control" name="apellido">Last Name</label>
	       	  		<input class="form-control" type="text" name="apellido" value="<?= $oldApellido ?>">
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control" name="email">Email</label>
	       	  		<input class="form-control" type="text" name="email" value="<?= $oldEmail ?>">
					<ul style="color:#5bd5db">
				<?php
                    if (!empty($Errores)) {
                    	foreach ($Errores as $Error) {
                            if ($Error == "Tienes que poner un email valido") {
                                echo "<li>$Error</li>";
                            }
                        }
                    }
           		?>
				   </ul>
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control" name="subject">Subject</label>
	       	  		<input class="form-control" type="text" name="subject" value="<?= $oldSubject ?>">
					<ul style="color:#5bd5db">
				 <?php
                    if (!empty($Errores)) {
                    	foreach ($Errores as $Error) {
                            if ($Error == "Tienes que poner el tema") {
                                echo "<li>$Error</li>";
                            }
                        }
                    }
           		?>
				   </ul>
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control" name="mensa">Message</label>
	       	  		<textarea class="form-control" name="mensa"><?= $oldMessage ?></textarea>
	       	  		<button class="pull-right btn btn-lg sr-button">SEND</button>
	       	  	</div>
	       	  </div>
	       </form>
		   <ul style="color:#5bd5db">
		   <?php
           if (empty($Errores)) {
			   echo "<li>Nombre: $oldNombre</li>";
			   echo "<li>Apellido: $oldApellido</li>";
			   echo "<li>Email: $oldEmail</li>";
			   echo "<li>Subject: $oldSubject</li>";
			   echo "<li>Mensaje: $oldMessage</li>";
           }
           ?>
		   </ul>
	       <hr class="divider">
	       <div class="address">
	           <h3>GET IN TOUCH</h3>
	           <hr>
	           <p>Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero.</p>
		       <div class="ending text-center">
			        <ul class="list-inline social-buttons">
			            <li><a href="#"><i class="fa fa-facebook sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-twitter sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-google-plus sr-icons"></i></a>
			            </li>
			        </ul>
				    <ul class="list-inline contact">
				       <li class="footer-number"><i class="fa fa-phone sr-icons"></i>  (00228)92229954 </li>
				       <li><i class="fa fa-envelope sr-icons"></i>  kouvenceslas93@gmail.com</li>
				    </ul>
				    <p>Photography Fanatic Template &copy; 2017</p>
		       </div>
	       </div>
	    </div>   
   	  </div>
   </div>
<!-- Principal Content Start -->

<?php include __DIR__."/partials/fin-doc.part.php"; ?>   