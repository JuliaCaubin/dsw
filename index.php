<?php
require __DIR__. "/utils/utils.php";
require __DIR__."/database/Connection.php";
require __DIR__. "/entity/ImagenGaleria.php";
require __DIR__."/exceptions/AppExceptions.php";
require __DIR__."/core/App.php";
require __DIR__."/database/QueryBuilder.php";
require __DIR__."/repository/ImagenGaleriaRepository.php";


$config =require_once("app/config.php");
    App::bind("config", $config);
$imagenGaleriaRepository = new ImagenGaleriaRepository();
$imagenes = $imagenGaleriaRepository->findAll();

require "views/index.view.php";
